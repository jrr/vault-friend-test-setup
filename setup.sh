#!/usr/bin/env bash

set -ueo pipefail

echo 'check for bins'

if ! which vault >/dev/null 2>&1; then
  echo 'vault binary not found in PATH. bye.'
  exit 1
fi

echo 'check for vars'

if [[ "${VAULT_ADDR:-}" == "" ]]; then
  echo 'Missing VAULT_ADDR env var'
  exit 1
fi

export VAULT_FORMAT=json

if [[ "${VAULT_TOKEN:-}" == "" ]]; then
  echo 'Missing VAULT_TOKEN env var'
  exit 1
fi

echo 'check for k8s token'

if [[ ! -d /var/run/secrets/kubernetes.io/serviceaccount ]]; then
  echo "this runtime doesn't look like k8s. bye."
  exit 1
fi

if [[ ! /var/run/secrets/kubernetes.io/serviceaccount/ca.crt ]]; then
  echo "can't find a CA cert for k8s. bye."
  exit 1
fi

if [[ ! /var/run/secrets/kubernetes.io/serviceaccount/token ]]; then
  echo 'no serviceaccount token mounted in. bye.'
  exit 1
fi

echo 'activate vault k8s auth'

vault auth enable kubernetes

echo 'config vault k8s auth'

k8s_ca=$(cat /var/run/secrets/kubernetes.io/serviceaccount/ca.crt)
k8s_token=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)

vault write auth/kubernetes/config \
  token_reviewer_jwt="${k8s_token}" \
  kubernetes_host=https://kubernetes.default.svc \
  kubernetes_ca_cert="${k8s_ca}" \
  disable_iss_validation=true

echo 'create vault-friend metadata policy'

vault policy write vault-friend-metadata vault-friend-metadata.hcl

echo 'create vault-friend k8s auth role'

vault write auth/kubernetes/role/vault-friend \
  bound_service_account_names=default \
  bound_service_account_namespaces=vault-friend-system \
  policies=default,vault-friend-metadata

echo 'create app policies'

vault policy write e2e-basic-test-app policy-basic.hcl
vault policy write e2e-pinning-test-app policy-pinning.hcl

echo 'create k8s auth roles'

vault write auth/kubernetes/role/e2e-basic-test-app \
  bound_service_account_names=test-app \
  bound_service_account_namespaces=e2e-basic \
  policies=default,e2e-basic-test-app

vault write auth/kubernetes/role/e2e-pinning-test-app \
  bound_service_account_names=test-app \
  bound_service_account_namespaces=e2e-pinning \
  policies=default,e2e-pinning-test-app

echo 'basic test data'

vault kv put secret/e2e-basic/test-app word=up
vault kv put secret/e2e-basic/test-tls tls.crt=@tls.crt tls.key=@tls.key

echo 'pinning test data'

vault kv put secret/e2e-pinning/test-app "salt-n-pepa=here"
vault kv put secret/e2e-pinning/test-app "salt-n-pepa=in-effect"

echo 'done!'
