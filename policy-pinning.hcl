path "secret/metadata/e2e-pinning/test-app" {
  capabilities = ["list"]
}

path "secret/data/e2e-pinning/test-app" {
  capabilities = ["read"]
}
