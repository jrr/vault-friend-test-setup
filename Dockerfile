FROM vault:1.7.3

RUN apk add --no-cache bash

COPY setup.sh /
COPY *.hcl /
COPY tls.* /

CMD /setup.sh
