path "secret/metadata/e2e-basic/*" {
  capabilities = ["list"]
}

path "secret/data/e2e-basic/*" {
  capabilities = ["read"]
}
