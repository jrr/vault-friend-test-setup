path "secret/metadata/" {
  capabilities = ["list"]
}

path "secret/metadata/*" {
  capabilities = ["list", "read"]
}
