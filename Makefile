IMG ?= quay.io/jrr/vault-friend-test-setup
VERSION ?= $(shell git rev-parse --short HEAD)

build:
	docker build -t $(IMG):$(VERSION) .

push: build
	docker push $(IMG):$(VERSION)

.PHONY: build push
